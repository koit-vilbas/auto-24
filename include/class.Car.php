<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 28.03.2018
 * Time: 11:57
 */

class Car extends DatabaseQuery
{
    protected static $table_name = "cars";
    protected static $table_fields = [
        'ID','url', 'name', 'power', 'doorCount', 'color', 'price', 'mainPicture', 'ownerID', 'added', 'addedBy', 'changedBy', 'status'
    ];

    public $ID,
        $url,
        $name,
        $power,
        $doorCount,
        $color,
        $price,
        $mainPicture,
        $ownerID,
        $added,
        $addedBy,
        $changedBy,
        $status;


    public function saveCategories($categories = []) {

        Rel::deleteByCar($this->ID);
        if(empty($categories)) {
            return;
        }

        foreach ($categories as $key => $category) {
            $rel = new Rel();
            $rel->carID = $this->ID;
            $rel->categoryID = $key;
            $rel->value = $category;
            $rel->added = date("Y-m-d H:i:s");
            $rel->addedBy = 1;
            $rel->changedBy = 1;
            $rel->status = 1;

            $rel->save();
        }

        return true;
    }

    public function getCategories() {

        $sql = "SELECT * FROM " . Rel::tableName() . " WHERE carID = " . $this->ID;

        $results = Rel::find_by_query($sql);

        return empty($results) ? false : $results;
    }
    public static function findByName($n, int $pageNo = 0) {
        global $database;
        global $options;

        if(empty($pageNo)) {
            $sql = "SELECT * FROM " . self::$table_name
                . " WHERE name LIKE '%" . $database->escape_value($n) ."%'";
        } else {
            $noOfItems = $options['page_items_no'];

            $offset = ($pageNo - 1) * $noOfItems;

            $sql = "SELECT * FROM " . self::$table_name
                . " WHERE name LIKE '%" . $database->escape_value($n) ."%' LIMIT {$offset},{$noOfItems}";
        }

        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }

    public static function getMainPicturePath($id) {
        global $database;

       $sql = "SELECT mainPicture FROM " . self::$table_name . " WHERE ID = " . $database->escape_value($id);

       $results = $database->query($sql);
       $result = $database->fetch_array($results);
       return empty($result) ? false : $result['mainPicture'];
    }

    public function makeUrl() {
        $url = makeUrl($this->name);

        $oCar = self::getByUrl($url);

        while (is_object($oCar) && $this->ID != $oCar->ID) {
            $string = "qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM1234567890";
            $stringRandom = str_shuffle($string);
            $hash = substr($stringRandom, 0, 3);

            $url = makeUrl($this->name) . "-" . $hash;
            $oCar = self::getByUrl($url, $this->ID);
        }

        return $url;
    }

    public static function getByUrl($url) {
        global $database;

        $sql = "SELECT * FROM " . self::$table_name
            . " WHERE url = '" . $database->escape_value($url) ."'";

        $results = self::find_by_query($sql);

        return empty($results) ? false : array_shift($results);
    }

    public function getPictures() {
        $picturesPath = UPLOAD_PATH . $this->ID . DS;
        $picturesPathMedium = UPLOAD_PATH . $this->ID . DS . MEDIUM . DS;
        $picturesPathThumb = UPLOAD_PATH . $this->ID . DS . THUMB . DS;

        $pictures = [
            'full' => glob($picturesPath . "/*.{jpg,png}", GLOB_BRACE),
            'medium' => glob($picturesPathMedium . "/*.{jpg,png}", GLOB_BRACE),
            'thumb' => glob($picturesPathThumb . "/*.{jpg,png}", GLOB_BRACE),
        ];
        return $pictures;
    }

    public function delete() {
        global $database;

        //Delete pictures

        $mainPath = UPLOAD_PATH . $this->ID . DS;
        recurseRmdir($mainPath);

        $sql = "DELETE FROM " . static::$table_name . " WHERE ID=" . $database->escape_value($this->ID);

        $database->query($sql);

//        return empty(Car::find($this->ID));

        return $database->check_last_query() == 1 ? true : false;
    }




}