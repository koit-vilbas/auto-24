<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 21.03.2018
 * Time: 12:33
 */

class Category extends DatabaseQuery
{
    protected static $table_name = 'categories';
    protected static $table_fields = [
        'ID', 'name', 'parentID', 'added', 'addedBy', 'changedBy', 'status'
    ];

    public $ID,
        $name,
        $parentID,
        $added,
        $addedBy,
        $changedBy,
        $status;


    public function getChildren() {
        $sql = 'SELECT * FROM ' . self::$table_name . ' WHERE parentID = ' . $this->ID;

        $results = self::find_by_query($sql);

        return !empty($results) ? $results : false;
    }

    public function getRel() {
        $sql = "SELECT * FROM " . Rel::tableName() . " WHERE categoryID = " . $this->ID;

        $results = Rel::find_by_query($sql);

        return empty($results) ? false : $results;
    }

    public static function findByName($n, $pageNo = 1) {
        global $database;
        global $options;

        if(empty($pageNo)) {
            $pageNo = 1;
        }

        $noOfItems = $options['page_items_no'];

        $offset = ($pageNo - 1) * $noOfItems;

        $sql = "SELECT * FROM " . self::$table_name
            . " WHERE name LIKE '%" . $database->escape_value($n) ."%' LIMIT {$offset},{$noOfItems}";

        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }

    public static function findByLimit($start, $max = MAX_ROWS) {
        global $database;
        $sql = "SELECT * FROM " . static::$table_name . " LIMIT " . $database->escape_value($start) . "," . $database->escape_value($max);

        $result = static::find_by_query($sql);

        return !empty($result) ? $result : false;
    }

    public function saveTranslation($translations = []) {

        Translations::deleteTranslationsByIdAndType($this->ID, 'category');
        global $session;

        if (empty($translations)) {
            return false;
        }

        foreach ($translations as $lang => $translation) {
            if (!empty($translation)) {
                $t = new Translations();
                $t->type = 'category';
                $t->typeID = $this->ID;
                $t->name = 'name';
                $t->translation = $translation;
                $t->language = $lang;
                $t->added = date("Y-m-d H:i:s");
                $t->addedBy = 1;
                $t->changedBy = 1;
                $t->status = 1;

                if($t->save()) {
                    $session->message('Translation successfully saved.');
                } else {
                    $session->message('Translation not saved.');
                }
            }
        }

        return true;
    }

}