<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 21.03.2018
 * Time: 12:33
 */

class CategoryRelation extends DatabaseQuery
{
    protected static $table_name = 'categories';
    protected static $table_fields = [
        'ID', 'name', 'value', 'categoryID'
    ];

    public $ID,
        $name,
        $value,
        $categoryID;



    public static function byCar( Car $car ) {
        $sql = "SELECT categories.ID, categories.name, rels.value, rels.categoryID FROM ".Rel::tableName()." JOIN ".Category::tableName()." ON ".Category::tableName().".ID = ".Rel::tableName().".categoryID WHERE carID = " . $car->ID;



        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }

}