<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 11.04.2018
 * Time: 13:23
 */


class Database extends PDO
{
    private $dbConfig = [
        'host' => 'localhost',
        'username' => 'kv-auto24',
        'password' => '3wXV6CAY8WjCof5k',
        'db' => 'kv-auto24',
        'port' => 3306
    ];

    private $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    public function __construct()
    {
        $dns = 'mysql' .
            ':host=' .  $this->dbConfig['host'] .
            ((!empty($this->dbConfig['port'])) ? (';port=' . $this->dbConfig['port']) : '3306') .
            ';dbname=' . $this->dbConfig['db'];

        parent::__construct($dns, $this->dbConfig['username'], $this->dbConfig['password'], $this->opt);
    }
}

$pdo = new Database();