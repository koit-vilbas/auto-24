<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 13.06.2018
 * Time: 13:42
 */

class Make extends DatabaseQuery
{
    protected static $table_name = "make";
    protected static $table_fields = [
        'ID', 'code', 'title'
    ];

    public $ID,
        $code,
        $title;



    public static function get($makeIDs = []) {
        global $database;

        $sql = "SELECT ID,code,title FROM " . self::$table_name;

        if(!empty($makeIDs)) {
            $sql .= " WHERE ID IN (". join($makeIDs) . ")";
        }

        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }
}