<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 13.06.2018
 * Time: 13:42
 */

class Model extends DatabaseQuery
{
    protected static $table_name = "model";
    protected static $table_fields = [
        'ID', 'makeID', 'code', 'title'
    ];

    public $ID,
        $makeID,
        $code,
        $title;



    public static function get($ID = 0) {
        global $database;

        $sql = "SELECT ID,title FROM " . self::$table_name;

        if(!empty($ID)) {
            $sql .= " WHERE ID = " . $database->escape_value($ID);
        }

        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }

    public static function findByMake($makeIDs) {
        global $database;

        $sql = "SELECT ID,title FROM " . self::$table_name;

        if(!empty($makeIDs)) {
            if(!is_array($makeIDs)) {
                $sql .= " WHERE makeID = " . $database->escape_value($makeIDs);
            } else {
                $sql .= " WHERE makeID IN (" . $database->escape_value(join(',', $makeIDs)) . ")";
            }
        }

        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }
}