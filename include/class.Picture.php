<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 23.05.2018
 * Time: 12:54
 */

class Picture
{

    public function make_thumb($src, $dest, $newHeight) {
        $sourceImage = $this->image_create_from_any($src);

        $oldWidth = imagesx($sourceImage);
        $oldHeight = imagesy($sourceImage);

        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $newWidth = floor($oldWidth * ($newHeight / $oldHeight));

        /* create a new, "virtual" image */
        $virtualImage = imagecreatetruecolor($newWidth, $newHeight);

        /* copy source image at a resized size */
        imagecopyresampled($virtualImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);
        /* create the physical thumbnail image to its destination */
        imagejpeg($virtualImage, $dest);
    }

    public function image_create_from_any($filepath) {
        $type = exif_imagetype($filepath); // [] if you don't have exif you could use getImageSize()
        $allowedTypes = array(
            1, // [] gif
            2, // [] jpg
            3, // [] png
            6   // [] bmp
        );
        if (!in_array($type, $allowedTypes)) {
            return false;
        }
        switch ($type) {
            case 1 :
                $im = imagecreatefromgif($filepath);
                break;
            case 2 :
                $im = imagecreatefromjpeg($filepath);
                break;
            case 3 :
                $im = imagecreatefrompng($filepath);
                break;
            case 6 :
                $im = imagecreatefrombmp($filepath);
                break;
        }
        return $im;
    }

    public function resizePicture($pictureName, $pictureLocationMain) {
        $this->make_thumb($pictureLocationMain . $pictureName, $pictureLocationMain . THUMB . DS . $pictureName, THUMB_SIZE);
        $this->make_thumb($pictureLocationMain . $pictureName, $pictureLocationMain . MEDIUM . DS . $pictureName, MEDIUM_SIZE);
        $this->make_thumb($pictureLocationMain . $pictureName, $pictureLocationMain . $pictureName, FULL_SIZE);
    }

    public function picturePathToUrl ($path) {
        if (empty($path)) {
            return "https://via.placeholder.com/200x200?text=Image+is+missing";
        }

        if (!file_exists($path)) {
            return "https://via.placeholder.com/200x200?text=Image+is+missing";
        }

        return str_replace(UPLOAD_PATH, UPLOAD_URL, $path);

    }
}
