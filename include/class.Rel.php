<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 11.04.2018
 * Time: 13:08
 */

class Rel extends DatabaseQuery
{
    protected static $table_name = 'rels';
    protected static $table_fields = [
        'ID', 'carID', 'categoryID', 'value', 'added', 'addedBy', 'changedBy', 'status'
    ];

    public $ID,
        $carID,
        $categoryID,
        $value,
        $added,
        $addedBy,
        $changedBy,
        $status;

    public static function deleteByCar($carID = 0) {
        $sql = 'DELETE FROM ' . self::$table_name. ' WHERE carID = ' . $carID;

        global $database;
        $database->query($sql);
        return $database->check_last_query() == 1 ? true : false;
    }

    public static function tableName() {
        return self::$table_name;
    }
}