<?php

/**
 * Created by PhpStorm.
 * User: janek.mander
 * Date: 10.05.2016
 * Time: 9:01
 */
class Session
{
    private $logged_in = false;
    public $user_id;
    public $message;
    public $language;
    public $group;

    function __construct() {
        session_start();
        $this->check_login();
        $this->check_message();
//        $this->setLanguage('et');

        if(!empty($_SESSION['language']))
            $this->language = $_SESSION['language'];

        // Enda lisatud
        if (!isset($_SESSION['initiated'])) {
            $_SESSION['initiated'] = TRUE;
        }
    }

    public function is_logged_in() {
        return $this->logged_in;
    }

    private function check_login() {
        if(isset($_SESSION['user_id'])) {

            $this->user_id = $_SESSION['user_id'];
            $this->group = $_SESSION['userGroup'];
            $this->logged_in = true;
        } else {
            unset($this->user_id);
            $this->logged_in = false;
        }
    }

    private function check_message() {
        if(isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }

    public function message ($msg = ""){
        if(empty($msg)) {
            return $this->message;
        } else {
            $_SESSION['message'] = $msg;
        }
    }

    public function login($user) {
        if($user) {
            $this->user_id = $_SESSION['user_id'] = $user->ID;
            $this->group = $_SESSION['userGroup'] = $user->userGroup;
            $this->logged_in = true;
        }
    }

    public function logout() {
        session_unset();
        unset($this->user_id);
        $this->logged_in = false;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        if(!empty($this->language)) {
            return $this->language;
        } else {
            return false;
        }
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language = 'en')
    {
        $_SESSION['language'] = $language;
        $this->language = $_SESSION['language'];
    }

    public function checkRights() {
        $userPagesWhiteList = ['car-list', 'car-add', 'car-edit', 'home'];
        $moderatorBlackList = ['user-list', 'user-add', 'user-edit'];
        $p = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);
        $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

        if ($this->group == 'admin') {
            return true;
        } else if ($this->group == 'moderator') {
            if (!in_array($p, $moderatorBlackList)) {
                return true;
            }
        } else if ($this->group == 'user') {
            if (in_array($p, $userPagesWhiteList)) {

                if ($p == 'car-edit') {
                    $car = Car::find($ID);
                    if ($car->added_by == $this->user_id) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }

        return false;
    }
}

$session = new Session();
$message = $session->message();