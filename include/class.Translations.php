<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 30.05.2018
 * Time: 14:20
 */

class Translations extends DatabaseQuery
{
    protected static $table_name = "translations";
    protected static $table_fields = [
        'ID','type', 'typeID', 'language', 'name', 'translation', 'added', 'addedBy', 'changedBy', 'status'
    ];

    public $ID,
        $type,
        $typeID,
        $language,
        $name,
        $translation,
        $added,
        $addedBy,
        $changedBy,
        $status;

    public static function getTranslations($obj, $type, $language = '') {
        global $database, $session;

        if(!is_object($obj)) {
//            $session->message('First variable must be an object.');
            return 0;
        }
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type = '" . $database->escape_value($type) . "' AND typeID = " . $obj->ID;


        if(!empty($language)) {
            $langSQL = " AND language = '" . $database->escape_value($language) . "'";
            $sql .= $langSQL;
        }

        $results = self::find_by_query($sql);
        return empty($results) ? false : $results;
    }

    public static function deleteTranslationsByIdAndType($ID, $type) {
        global $database;

        $sql = "DELETE FROM " . self::$table_name
            . " WHERE typeID=" . $database->escape_value($ID)
            . " AND type='" . $type . "'";

        $database->query($sql);

        return $database->check_last_query() == 1 ? true : false;
    }

    public static function getLanguages() {

        $sql = "SELECT DISTINCT(language) FROM translations";

        $results = self::find_by_query($sql);
        return empty($results) ? false : $results;
    }
}