<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 15.03.2018
 * Time: 10:26
 */

class User extends DatabaseQuery
{
    protected static $table_name = "users";
    protected static $table_fields = [
        'ID', 'username', 'password', 'userGroup', 'email', 'name', 'birthDate', 'added', 'addedBy', 'changedBy', 'status'
    ];

    public $ID,      //    INT(11)
        $username, //    VARCHAR(255)
        $password, //    VARCHAR(60)
        $userGroup,   //    ENUM('admin', 'moderator', 'user')
        $email,    //    VARCHAR(255)
        $name,     //    VARCHAR(255)
        $birthDate,//    DATE
        $added,   //    DATETIME
        $addedBy,  //    INT(11)
        $changedBy,//    INT(11)
        $status;   //    INT(1)

    public static function findByUsername($username) {
        global $database;
        $sql = 'SELECT * FROM ' . self::$table_name . ' WHERE username="'. $database->escape_value($username) .'"';
        $results = self::find_by_query($sql);

        return empty($results) ? false : array_shift($results);
    }

    public static function searchByUsername($username, int $pageNo = 0) {
        global $database;

        global $options;

        if(empty($pageNo)) {
            $pageNo = 1;
        }

        $noOfItems = $options['page_items_no'];

        $offset = ($pageNo - 1) * $noOfItems;

        $sql = 'SELECT * FROM ' . self::$table_name . ' WHERE username LIKE "%'. $database->escape_value($username) .'%" LIMIT '.$offset.','.$noOfItems;
        $results = self::find_by_query($sql);

        return empty($results) ? false : $results;
    }

    public function auth($password) {
        if(password_verify($password, $this->password)) {
            return true;
        } else {
            return false;
        }
    }
}