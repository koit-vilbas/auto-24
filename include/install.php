<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 15.03.2018
 * Time: 11:46
 */

$create_db = "CREATE SCHEMA IF NOT EXISTS `kv-auto24` DEFAULT CHARACTER SET utf8 ;
USE `kv-auto24` ;";

$add_users_table[] = "CREATE TABLE `users` (
`ID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `group` enum('admin','moderator','user') NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthDate` date NOT NULL,
  `added` datetime NOT NULL,
  `addedBy` int(11) NOT NULL,
  `changed` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `changedBy` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$add_users_table[] = "ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);";

$add_users_table[] = "ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;";

$add_categories_table = "CREATE TABLE IF NOT EXISTS `kv-auto24`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `parentID` INT(11) NOT NULL,
  `added` DATETIME NOT NULL,
  `addedBy` INT(11) NOT NULL,
  `changed` DATETIME NOT NULL,
  `changedBy` INT(11) NOT NULL,
  `status` INT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;";

$rel_table = "CREATE TABLE IF NOT EXISTS `kv-auto24`.`rels` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `carID` INT(11) NOT NULL,
  `categoryID` INT(11) NOT NULL,
  `cat_parentID` INT(11) NOT NULL,
  `added` DATETIME NOT NULL,
  `addedBy` INT(11) NOT NULL,
  `changed` DATETIME NOT NULL,
  `changedBy` INT(11) NOT NULL,
  `status` INT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;";