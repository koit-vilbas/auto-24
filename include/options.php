<?php
/**
* Created by PhpStorm.
* User: koit.vilbas
* Date: 15.03.2018
* Time: 11:10
*/

$options = [
    'page_items_no' => 5
];

$adminPages = [
    'home' => [
        'title' => t('Admin Dashboard', true),
        'path' => ADMIN_PAGES_PATH . 'users/home.php',
        'showOnMenu' => 1
    ],
    'user-add' => [
        'title' => t('Add New User', true),
        'path' => ADMIN_PAGES_PATH . 'users/addAndEdit.php',
        'showOnMenu' => 0
    ],
    'user-edit' => [
        'title' => t('Edit User', true),
        'path' => ADMIN_PAGES_PATH . 'users/addAndEdit.php',
        'showOnMenu' => 0
    ],
    'user-list' => [
        'title' => t('Users', true),
        'path' => ADMIN_PAGES_PATH . 'users/list.php',
        'showOnMenu' => 1
    ],
    'user-delete' => [
        'title' => t('Delete User', true),
        'path' => ADMIN_PAGES_PATH . 'users/delete.php',
        'showOnMenu' => 0
    ],
    'cat-add' => [
        'title' => t('Add category', true),
        'path' => ADMIN_PAGES_PATH . 'categories/addEdit.php',
        'showOnMenu' => 0
    ],
    'cat-edit' => [
        'title' => t('Edit category', true),
        'path' => ADMIN_PAGES_PATH . 'categories/addEdit.php',
        'showOnMenu' => 0
    ],
    'cat-list' => [
        'title' => t('Categories', true),
        'path' => ADMIN_PAGES_PATH . 'categories/list.php',
        'showOnMenu' => 1
    ],
    'cat-delete' => [
        'title' => t('Delete categories', true),
        'path' => ADMIN_PAGES_PATH . 'categories/delete.php',
        'showOnMenu' => 0
    ],
    'car-add' => [
        'title' => t('Add car', true),
        'path' => ADMIN_PAGES_PATH . 'cars/addEdit.php',
        'showOnMenu' => 0
    ],
    'car-edit' => [
        'title' => t('Edit Car', true),
        'path' => ADMIN_PAGES_PATH . 'cars/addEdit.php',
        'showOnMenu' => 0
    ],
    'car-list' => [
        'title' => t('Cars', true),
        'path' => ADMIN_PAGES_PATH . 'cars/list.php',
        'showOnMenu' => 1
    ],
    'car-delete' => [
        'title' => t('Delete Cars', true),
        'path' => ADMIN_PAGES_PATH . 'cars/delete.php',
        'showOnMenu' => 0
    ],
    'car-gallery' => [
        'title' => t('Car Gallery', true),
        'path' => ADMIN_PAGES_PATH . 'cars/gallery.php',
        'showOnMenu' => 0,
        'js' => ADMIN_PAGES_URL . 'cars/js/car.js'
    ],
    'login' => [
        'title' => t('Login', true),
        'path' => ADMIN_PATH . 'login.php',
        'showOnMenu' => 0
    ]
];

