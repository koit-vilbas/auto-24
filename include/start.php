<?php
/**
 * Created by PhpStorm.
 * User: janek.mander
 * Date: 10.05.2016
 * Time: 9:02
 */
//If page is LIVE
//error_reporting(0);
error_reporting(E_ALL);
ini_set('display_errors', 'On');

defined('DS') ? NULL : define('DS', DIRECTORY_SEPARATOR);

//DIRECTORY_SEPARATOR
//Windows \
//Linux /

//CONSTANTS FOR PROJECT

/*
 * CHANGE TWO LINES 20 AND 21-22
 * */
defined('ROOT_URL') ? null : define('ROOT_URL', 'https://auto24.test/');
defined('ROOT_PATH') ? null :
    define('ROOT_PATH',DS.'home'.DS.'koit'.DS.'PhpstormProjects'.DS.'auto24'.DS);

//MAIN CONSTANTS
defined('INCLUDE_PATH') ? null : define('INCLUDE_PATH', ROOT_PATH . 'include' . DS);
defined('MAIN_URL') ? null : define('MAIN_URL', ROOT_URL);
defined('MAIN_PATH') ? null : define('MAIN_PATH', ROOT_PATH . 'public' . DS);

//ADMIN CONSTANTS
defined('ADMIN_PATH') ? null : define('ADMIN_PATH', MAIN_PATH . 'admin/');
defined('ADMIN_PAGES_PATH') ? null : define('ADMIN_PAGES_PATH', ADMIN_PATH . 'pages/');

defined('ADMIN_URL') ? null : define('ADMIN_URL', MAIN_URL . 'admin/');
defined('ADMIN_PAGES_URL') ? null : define('ADMIN_PAGES_URL', ADMIN_URL . 'pages/');

defined('LOGIN_URL') ? null : define('LOGIN_URL', ADMIN_URL . 'login.php');
defined('LOGOUT_URL') ? null : define('LOGOUT_URL', ADMIN_URL . 'logout.php');

defined('CAR_URL') ? null : define('CAR_URL', MAIN_URL . 'car' . DS);

//TEMPLATE CONSTANTS
defined('TEMPLATE_PATH') ? null : define('TEMPLATE_PATH', MAIN_PATH . 'template' . DS);
defined('TEMPLATE_URL_CSS') ? null : define('TEMPLATE_URL_CSS', MAIN_URL . 'template/css/');
defined('TEMPLATE_URL_JS') ? null : define('TEMPLATE_URL_JS', MAIN_URL . 'template/js/');

defined('UPLOAD_PATH') ? null : define('UPLOAD_PATH', MAIN_PATH . 'upload' . DS);
defined('UPLOAD_URL') ? null : define('UPLOAD_URL', MAIN_URL . 'upload/');
defined('THUMB') ? null : define('THUMB', 'thumb');
defined('MEDIUM') ? null : define('MEDIUM', 'medium');
defined('FULL_SIZE') ? null : define('FULL_SIZE', '1024');
defined('MEDIUM_SIZE') ? null : define('MEDIUM_SIZE', '600');
defined('THUMB_SIZE') ? null : define('THUMB_SIZE', '200');

defined('MAX_ROWS') ? null : define('MAX_ROWS', '10');

defined('TRANSLATIONS_PATH') ? null : define('TRANSLATIONS_PATH', ROOT_PATH . '/translations/');


//Dynamic classes
require_once INCLUDE_PATH . 'class.MySQLDatabase.php';
require_once INCLUDE_PATH . 'class.DatabaseQuery.php';
require_once INCLUDE_PATH . 'class.Session.php';
require_once INCLUDE_PATH . 'class.User.php';
require_once INCLUDE_PATH . 'class.Category.php';
require_once INCLUDE_PATH . 'class.CategoryRelation.php';
require_once INCLUDE_PATH . 'class.Car.php';
require_once INCLUDE_PATH . 'class.Picture.php';
require_once INCLUDE_PATH . 'class.Translations.php';
require_once INCLUDE_PATH . 'class.Rel.php';
require_once INCLUDE_PATH . 'class.Make.php';
require_once INCLUDE_PATH . 'class.Model.php';
require_once INCLUDE_PATH . 'functions.php';

if(!$session->getLanguage()) {
    $session->setLanguage('et');
}


if($session->getLanguage() != false) {
    $p = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);
    $p = empty($p) || $p == 'car' ? 'public.php' : 'admin.php';

    require_once TRANSLATIONS_PATH . $session->language . DS . $p;
}

require_once INCLUDE_PATH . 'options.php';
