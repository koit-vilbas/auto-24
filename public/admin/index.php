<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 15.03.2018
 * Time: 10:59
 */
ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once "../../include/start.php";

if(!$session->is_logged_in()) {
    redirect(ADMIN_URL . 'login.php');
    exit();
}

if (!$session->checkRights()) {
    redirect(ADMIN_URL . 'home');
    exit();
}

$p = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);



if(empty($p)) {
    redirect(ADMIN_URL . 'home');
    exit();
}
get_template('head');

?>

<div class="container">
    <div class="row">
        <div class="col-8">
            <h2 class="display-4"><?php echo $adminPages[$p]['title']; ?></h2>
        </div>
        <div class="col-2">
            <select name="language" id="language" class="form-control">
                <option value="et" <?php echo $session->getLanguage() === 'et' ? 'selected' : '' ?>><?php t('estonian') ?></option>
                <option value="en" <?php echo $session->getLanguage() === 'en' ? 'selected' : '' ?>><?php t('english') ?></option>
            </select>
        </div>
        <div class="col-2 text-right">
            <a href="<?php echo LOGOUT_URL; ?>"><i class="fa fa-power-off" style="font-size: 24px;"></i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <ul class="nav nav-pills flex-column">
                <?php if(!empty($adminPages)): foreach ($adminPages as $key => $adminPage) {?>
                    <?php if($adminPage['showOnMenu'] === 1): ?>
                    <li class="nav-item"><a class="nav-link <?php echo $p == $key ? 'active' : ''; ?>" href="<?php echo ADMIN_URL . $key?>"><?php echo $adminPage['title']; ?></a></li>
                    <?php endif; ?>
                <?php } endif;?>
            </ul>
        </div>
        <div class="col-8">
            <?php if(!empty($p) && isset($adminPages[$p])): ?>
                <?php require_once $adminPages[$p]['path']; ?>
            <?php else: ?>
                <?php require_once $adminPages['home']['path']; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
<?php get_template('footer', $adminPages[$p]); ?>



