<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 28.03.2018
 * Time: 10:24
 */

require_once "../../include/start.php";


$p = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);
get_template('head');

$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password');

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
echo $action;
if(isset($action) ) {
    $user = User::findByUsername($username);
    if($user->auth($password) && $user->status == 1) {
        $session->login($user);
        redirect(ADMIN_URL);
    } else {
        echo '<div class="alert alert-danger">Username or password is incorrect..</div>';
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="display-4"><?php echo $adminPages['login']['title']; ?></h2>
        </div>
    </div>
    <div class="row">
        <form class="form-signin" method="post">
            <h2 class="form-signin-heading">Please sign in</h2>
            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" id="inputEmail" name="username" class="form-control" placeholder="Username" required="" autofocus="">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="action" value="signin">Sign in</button>
        </form>
    </div>
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
<?php get_template('footer'); ?>



