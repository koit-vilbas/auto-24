<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 28.03.2018
 * Time: 11:01
 */

require_once "../../include/start.php";

$session->logout();

redirect(ADMIN_URL . 'login.php');