<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 28.03.2018
 * Time: 11:56
 */

    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $power = filter_input(INPUT_POST, 'power', FILTER_SANITIZE_STRING);
    $doorCount = filter_input(INPUT_POST, 'doorCount', FILTER_SANITIZE_STRING);
    $color = filter_input(INPUT_POST, 'color', FILTER_SANITIZE_STRING);
    $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
    $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);

    $args = [
        'category' => [
            'flags' => FILTER_REQUIRE_ARRAY
        ]
    ];
    $category = filter_input_array(INPUT_POST, $args);

//    pd($data, true);
    $carSubCategoriesId = [];
    if($p == 'car-edit') {
        if(empty($_GET['ID'])) {
            redirect(ADMIN_URL . 'car-list');
        }
        $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

        $car = Car::find($ID);

        $carSubCategories = $car->getCategories();
        if(is_array($carSubCategories)) {
            $carSubCategoriesId = array_column($carSubCategories, 'categoryID');
        }

    }

    if(isset($action) && $action === t('SUBMIT_BUTTON', true)) {
        $car = new Car();
        $car->name = $name;
        $car->url = $car->makeUrl();
        $car->power = $power;
        $car->doorCount = $doorCount;
        $car->color = $color;
        $car->price = $price;
        $car->ownerID = $session->user_id;
        $car->status = isset($status) && $status === 'on' ? 1 : 0;
        $car->added = date("Y-m-d H:i:s");
        $car->addedBy = $session->user_id;
        $car->changedBy = $session->user_id;





        if($car->save()) {
            $car->ID = $database->get_last_id();
            $car->saveCategories($category['category']);
            echo '<div class="alert alert-success">' . t('CAR_ADDED', true) . '</div>';
            redirect(ADMIN_URL . 'car-edit/' . $car->ID);
        } else {
            echo '<div class="alert alert-danger">' . t('CAR_NOT_ADDED', true) . '</div>';
        }
    }

if(isset($action) && $action === t('EDIT_BUTTON', true)) {
    $car->name = $name;
    $car->url = $car->makeUrl();
    $car->power = $power;
    $car->doorCount = $doorCount;
    $car->color = $color;
    $car->price = $price;
    $car->status = isset($status) && $status === 'on' ? 1 : 0;
    $car->changedBy = $session->user_id;;

    if($car->save() || $car->saveCategories($category['category'])) {
        echo '<div class="alert alert-success">' . t('CAR_EDITED', true) . '</div>';
    } else {
        echo '<div class="alert alert-danger">' . t('CAR_NOT_EDITED', true) . '</div>';
    }


}
$categories = Category::find_all();
?>
<div class="container">
    <a href="<?php echo ADMIN_URL . 'car-list'?>" class="btn btn-dark"><i class="fa fa-chevron-circle-left"></i><?php t('BACK_BUTTON'); ?></a>
    <form method="post">
        <div class="row form-group">
            <label for="addCar_name" class="col-2 col-form-label"><?php t('NAME'); ?>:</label>
            <div class="col-10">
                <input type="text" name="name" class="form-control" id="addCar_name" required value="<?php echo isset($car) ? $car->name : ''; ?>">
            </div>
        </div>
        <div class="row form-group">
            <label for="addCar_power" class="col-2 col-form-label"><?php t('CAR_POWER'); ?>(kW):</label>
            <div class="col-10">
                <input type="text" name="power" class="form-control" id="addCar_power" <?php echo $p === 'car-edit' ? '' : 'required'; ?> value="<?php echo isset($car) ? $car->power : ''; ?>">
            </div>
        </div>
        <div class="row form-group">
            <label for="addCar_doorCount" class="col-2 col-form-label"><?php t('CAR_DOOR_COUNT'); ?>:</label>
            <div class="col-10">
                <input type="text" name="doorCount" class="form-control" id="addCar_doorCount" value="<?php echo isset($car) ? $car->doorCount : ''; ?>">
            </div>
        </div>
        <div class="row form-group">
            <label for="addCar_color" class="col-2 col-form-label"><?php t('CAR_COLOR'); ?>:</label>
            <div class="col-10">
                <input type="text" name="color" class="form-control" id="addCar_color" value="<?php echo isset($car) ? $car->color : ''; ?>" >
            </div>
        </div>
        <div class="row form-group">
            <label for="addCar_price" class="col-2 col-form-label"><?php t('CAR_PRICE'); ?>:</label>
            <div class="col-10">
                <input type="text" name="price" class="form-control" id="addCar_price" value="<?php echo isset($car) ? $car->price : ''; ?>">
            </div>
        </div>


        <?php if(!empty($categories)): ?>
            <?php foreach($categories as $category): ?>
                <?php if($category->getChildren() == false && $category->parentID == 0 || true):?>
                <?php $translation = Translations::getTranslations($category, 'category', $session->getLanguage()); ?>
                <div class="row form-group">
                    <label for="addCar_<?php echo $translation[0]->translation; ?>" class="col-2 col-form-label"><?php echo $translation[0]->translation; ?></label>
                    <div class="col-10">
                        <?php $value = isset($car) ? Rel::find_by_query('SELECT * FROM rels WHERE carID = ' . $car->ID .' AND categoryID = ' . $category->ID) : []; ?>
                        <input type="text" name="category[<?php echo $category->ID; ?>]" class="form-control" id="addCar_<?php echo $translation[0]->translation; ?>" value="<?= array_key_exists(0, $value) ? $value[0]->value : '' ?>">
                    </div>
                </div>
                <?php //if(!empty($category->getChildren())) { pd($category->getChildren()); } ?>
                <?php elseif($category->getChildren() && false): ?>
                    <?php $translation = Translations::getTranslations($category, 'category', $session->getLanguage()); ?>
                    <?php $children = $category->getChildren(); ?>
                    <div class="row form-group">
                        <label for="addCar_<?php echo $translation[0]->translation; ?>" class="col-2 col-form-label"><?php echo $translation[0]->translation; ?></label>
                        <div class="col-10">
                            <select class="form-control" id="addCar_<?php echo $translation[0]->translation; ?>" name="category[<?= $category->ID ?>]">
                                <?php foreach($children as $child): ?>
                                    <option value="<?php echo $child->ID; ?>" <?php echo in_array($child->ID, $carSubCategoriesId) ? 'selected' : "" ?>><?php echo $child->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="status" <?php echo (isset($car) && $car->status == 1) || $p === 'car-add' ? 'checked' : ''; ?>>
                    <label for="status"><?php t('IS_ACTIVE'); ?>?</label>
                </label>
            </div>
        <?php endif; ?>
        <?php if(isset($p) && $p === 'car-edit'): ?>
            <input type="submit" name="action" class="btn btn-primary" value="<?php t('EDIT_BUTTON');?>">
        <?php else: ?>
            <input type="submit" name="action" class="btn btn-primary" value="<?php t('SUBMIT_BUTTON');?>">
        <?php endif; ?>
    </form>
</div>

