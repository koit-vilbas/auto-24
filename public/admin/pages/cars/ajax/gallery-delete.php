<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 23.05.2018
 * Time: 14:02
 */

require_once "../../../../../include/start.php";

$carID = filter_input(INPUT_POST, 'carID', FILTER_VALIDATE_INT);
$picturePath = filter_input(INPUT_POST, 'picturePath', FILTER_SANITIZE_STRING);

$car = Car::find($carID);

if (!is_object($car)) {
    header('Content-Type: application/json');
    echo  json_encode([
        'status' => 0,
        'message' => 'Car is missing!'
    ]);
    exit();
}

if (!file_exists($picturePath)) {
    header('Content-Type: application/json');
    echo json_encode([
        'status' => 0,
        'message' => 'Picture is missing!'
    ]);
    exit();
}

$mainPath = UPLOAD_PATH . $car->ID . DS;
$pictureName = explode("/", $picturePath);
$pName = end($pictureName);

unlink($mainPath . $pName);
unlink($mainPath . THUMB . DS . $pName);
unlink($mainPath . MEDIUM . DS . $pName);
header('Content-Type: application/json');
echo json_encode([
    'status' => 1,
    'message' => "Picture Deleted!"
]);
exit();