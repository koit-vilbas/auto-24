<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 23.05.2018
 * Time: 14:59
 */

require_once "../../../../../include/start.php";

$carID = filter_input(INPUT_POST, 'carID', FILTER_VALIDATE_INT);
$picturePath = filter_input(INPUT_POST, 'picturePath', FILTER_SANITIZE_STRING);

$car = Car::find($carID);

if (!is_object($car)) {
    exit( json_encode([
        'status' => 0,
        'message' => 'Car is missing!'
    ]));
}

if (!file_exists($picturePath)) {
    exit( json_encode([
        'status' => 0,
        'message' => 'Picture is missing!'
    ]));
}

$mainPath = UPLOAD_PATH . $car->ID . DS;
$pictureName = explode("/", $picturePath);
$pName = end($pictureName);

$car->mainPicture = $pName;
$car->save();

exit( json_encode([
    'status' => 1,
    'message' => "Picture is main!"
]));