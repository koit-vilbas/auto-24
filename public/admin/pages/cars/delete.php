<?php

if($p === 'car-delete') {
    if(empty($_GET['ID'])) {
        redirect(ADMIN_URL . 'car-list');
    }
    $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

    $car = Car::find($ID);


    if($car->delete() && Rel::deleteByCar($car->ID)) {
        $session->message('<div class="alert alert-success">'.t("Car deleted.", true).'</div>');
        redirect(ADMIN_URL . 'car-list', 0);
    } else {
        $session->message('<div class="alert alert-danger">'.t("Error: car not deleted.", true).'</div>');
    }

    redirect(ADMIN_URL . 'car-list', 0);
}