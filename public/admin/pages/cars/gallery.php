<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 23.05.2018
 * Time: 13:00
 */

$ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

if (empty($ID)) {
    redirect(ADMIN_URL . "car-list");
}
$car = Car::find($ID);
$picturesPathMain = UPLOAD_PATH . $car->ID . DS;
$picturesPathThumb = UPLOAD_PATH . $car->ID . DS . THUMB;
$picturesPathMedium = UPLOAD_PATH . $car->ID . DS . MEDIUM;

if (!file_exists($picturesPathMain)) {
    mkdir($picturesPathMain, 0755, true);
}

if (!file_exists($picturesPathThumb)) {
    mkdir($picturesPathThumb, 0755, true);
}

if (!file_exists($picturesPathMedium)) {
    mkdir($picturesPathMedium, 0755, true);
}

$btn = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
if (isset($btn) && $btn === 'upload') {

    $picturesPathMainExtension = $_FILES['file']['type'] === 'image/jpeg' ? 'jpg' : 'png';
    $picturesPathMainName = $ID . '_' . date("Y_m_d_H_i_s") . "." . $picturesPathMainExtension;
    $picturesPathMainPath = $picturesPathMain . DS . $picturesPathMainName;


    if ($_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/png') {
        if (move_uploaded_file($_FILES['file']['tmp_name'], $picturesPathMainPath)) {
            //TODO picture uploaded
            echo "Picture uploaded";
            (new Picture())->resizePicture($picturesPathMainName, $picturesPathMain);
            $session->message('<div class="alert alert-success">'.t("Image uploaded.", true).'</div>');
            redirect(ADMIN_URL.'car-gallery'.DS.$car->ID);
        } else {
            $session->message('<div class="alert alert-danger">'.t("Error: image upload failed.", true).'</div>');
        }
    } else {
        $session->message('<div class="alert alert-danger">'.t("Error: only jpeg and png are supported.", true).'</div>');
    }



}

$pictures = glob($picturesPathMain . "/*.{jpg,png}", GLOB_BRACE);

echo $session->message();
?>



<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="input-group mb-3">
            <input type="file" name="file" class="form-control">
            <div class="input-group-append">
                <button type="submit" name="action" value="upload" class="btn btn-success">Upload</button>
            </div>
        </div>
    </div>
</form>

<div class="row">
    <?php if (!empty($pictures)) : foreach ($pictures as $picture) { ?>
        <div class="col-sm" style="padding-top: 10px;">
            <img src="<?php echo (new Picture())->picturePathToUrl($picture); ?>" class="img-fluid shadow">
           <div class="btn-group" style="width:100%;">
               <button type="button" class="btn btn-primary top-border mainPicture" style="width:50%;" data-car-id="<?php echo $car->ID; ?>" data-picture-name="<?php echo $picture;?>">Main Picture</button>
               <button type="button" class="btn btn-danger top-border delete_picture" style="width:51%;" data-car-id="<?php echo $car->ID; ?>" data-picture-name="<?php echo $picture;?>">Delete</button>
           </div>
        </div>
    <?php } endif; ?>
</div>