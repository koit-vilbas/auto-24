$(".delete_picture").click(function () {
    var car_id = $(this).data("car-id");
    var picturePath = $(this).data("picture-name");
    var container = $(this).closest(".col-sm");

    $.ajax({
        url: mainUrl + "admin/pages/cars/ajax/gallery-delete.php",
        data: { carID: car_id, picturePath: picturePath },
        dataType: 'json',
        method: 'POST',
        success: function (response) {
            if(response.status == 0) {
                console.log('Error!');
            } else {
                container.remove();
            }
        }
    })
});

$(".mainPicture").click(function () {
    var car_id = $(this).data("car-id");
    var picturePath = $(this).data("picture-name");
    var container = $(this).closest(".col-sm");

    $.post(
        mainUrl + "admin/pages/cars/ajax/main-picture.php",
        { carID: car_id, picturePath: picturePath }
    ).done(function( data ) {
        var obj = JSON.parse(data);
        if (obj.status == 0) {
            console.log('Error!');
        }
    });
});