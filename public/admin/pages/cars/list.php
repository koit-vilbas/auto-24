<?php
$search = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);
$pageNumber = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);
$page = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);

if(empty($pageNumber))
    $pageNumber = 1;

$search = strtolower($search);

$pageCount = ceil(Car::count_all() / $options['page_items_no']) ;

$n = '';
if (empty($search)) {
    $n = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
    if (empty($n)) {
        $cars = Car::find_all((int)$pageNumber);
        $_SESSION['search'] = false;
    } else {
        $cars = Car::findByName($n, $pageNumber);
        $pageCount = ceil(count($cars) / $options['page_items_no']) ;
    }
} else {
    $_SESSION['search'] = true;
    redirect(ADMIN_URL . 'car-list'.DS.$pageNumber.DS.$search);
}
echo $session->message();



$nextPage = 0;
$prevPage = 0;
if(!empty($pageNumber)) {
    $nextPage = $pageNumber + 1;
    $prevPage = $pageNumber - 1;

    if($nextPage > $pageCount)
        $nextPage = $pageNumber;

    if($prevPage < 1)
        $prevPage = $pageNumber;
}

?>
<div class="row">
    <div class="col-12">
        <form method="POST" action="<?= ADMIN_URL . 'car-list'.DS.$pageNumber ?>">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Search..." name="search" value="<?php echo isset($n) ? $n : ""; ?>">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12 m-md-2">
        <a href="<?php echo ADMIN_URL . 'car-add/'?>"><button class="btn btn-outline-success" type="submit"><?php t('add_new_car'); ?></button></a>
    </div>
</div>
<div class="table-responsive">
    <table class="table">
        <thead>
            <th><?php t('NAME'); ?></th>
            <th><?php t('CAR_POWER'); ?></th>
            <th><?php t('CAR_DOOR_COUNT'); ?></th>
            <th><?php t('CAR_COLOR'); ?></th>
            <th><?php t('CAR_PRICE'); ?></th>
            <th><?php t('DELETE'); ?></th>
            <th><?php t('GALLERY'); ?></th>
        </thead>
        <tbody>
        <?php if(!empty($cars)) : foreach ($cars as $car) {?>
            <tr>
                <td><a href="<?php echo ADMIN_URL . 'car-edit/' . $car->ID; ?>"><?php echo $car->name; ?></td>
                <td><?php echo $car->power ?></td>
                <td><?php echo $car->doorCount; ?></td>
                <td><?php echo $car->color; ?></td>
                <td><?php echo $car->price; ?></td>
                <td><a href="<?php echo ADMIN_URL . 'car-delete/' . $car->ID?>"><i class="fa fa-trash"></i></a></td>
                <td><a href="<?php echo ADMIN_URL . 'car-gallery/' . $car->ID?>"><i class="fa fa-images"></i></a></td>
            </tr>
        <?php } endif; ?>
        </tbody>
    </table>
</div>
<nav aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$prevPage.DS.$n ?>"><?= t('previous') ?></a></li>
        <?php for($i=1;$i<=$pageCount;$i++):?>
        <li class="page-item <?= $pageNumber == $i ? 'active' : '' ?>"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$i.DS.$n ?>"><?= $i ?></a></li>
        <?php endfor; ?>
        <li class="page-item"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$nextPage.DS.$n ?>"><?= t('next') ?></a></li>
    </ul>
</nav>
