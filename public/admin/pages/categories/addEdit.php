<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 21.03.2018
 * Time: 12:57
 */

global $session;
$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
$args = [
    'name' => [
        'filter' => FILTER_SANITIZE_STRING,
        'flags' => FILTER_REQUIRE_ARRAY
    ]
];
$names = filter_input_array(INPUT_POST, $args);
$parent = filter_input(INPUT_POST, 'parent', FILTER_VALIDATE_INT);
$status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);


if($p === 'cat-edit') {
    if(empty($_GET['ID'])) {
        redirect();
    }
    $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

    $category = Category::find($ID);

    $translations = Translations::getTranslations($category, 'category');
}

if(isset($action) && $action === "Submit") {
    $category = new Category();
    $category->parentID = empty($parent) ? '0' : $parent;
    $category->status = isset($status) && $status === 'on' ? 1 : 0;
    $category->added = date("Y-m-d H:i:s");
    $category->addedBy = 1;
    $category->changedBy = 1;

    if($category->save() && $category->saveTranslation($names['name'])) {
        echo '<div class="alert alert-success">Category added</div>';
    } else {
        echo '<div class="alert alert-danger">Category NOT added</div>';
    }
}

if(isset($action) && $action === "Edit") {


    $category->parentID = empty($parent) ? '0' : $parent;
    $category->status = isset($status) && $status === 'on' ? 1 : 0;
    $category->changedBy = 1;
    if($category->ID != $parent) {
        if($category->save() || $category->saveTranslation($names['name'])) {
            echo '<div class="alert alert-success">Category edited</div>';
        } else {
            echo '<div class="alert alert-danger">Category NOT edited</div>';
        }
    } else {
        echo '<div class="alert alert-danger">Category can not be parent of itself.</div>';
    }


}

?>

<div class="container">
    <a href="<?php echo ADMIN_URL . 'cat-list'?>" class="btn btn-dark"><i class="fa fa-chevron-circle-left"></i><?php t('BACK_BUTTON'); ?></a>
    <form method="post">
        <?php if(!empty($translations) && $p === 'cat-edit') : foreach ($translations as $translation) { ?>
            <div class="row form-group">
                <label for="addCat_name" class="col-2 col-form-label"><?php t('NAME'); ?> [<?php echo strtoupper($translation->language); ?>]:</label>
                <div class="col-10">
                    <input type="text" name="name[<?php echo $translation->language; ?>]" class="form-control" id="addCat_name" required value="<?php echo isset($translation) ? $translation->translation : ''; ?>">
                </div>
            </div>
        <?php } else: ?>
            <?php $languages = Translations::getLanguages(); ?>
            <?php foreach ($languages as $language): ?>
                <div class="row form-group">
                    <label for="addCat_name" class="col-2 col-form-label"><?php t('NAME'); ?> [<?php echo strtoupper($language->language); ?>]:</label>
                    <div class="col-10">
                        <input type="text" name="name[<?php echo $language->language; ?>]" class="form-control" id="addCat_name" required>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <div class="row form-group">
            <label for="addCat_parent" class="col-2 col-form-label">Parent:</label>
            <div class="col-10">
                <select class="form-control" id="addCat_parent" name="parent">
                    <?php $categories = Category::find_all(); ?>
                    <option value="0">None</option>
                    <?php if(!empty($categories)): foreach($categories as $cat) {?>
                        <?php if($cat->parentID == 0):?>
                            <?php $translation = Translations::getTranslations($cat, 'category', $session->getLanguage()); ?>
                            <option value="<?php echo $cat->ID; ?>" <?php echo $cat->parentID == $cat->ID ? 'selected' : ''; ?>><?php echo $translation[0]->translation; ?></option>
                        <?php endif; ?>
                    <?php } endif; ?>
                </select>
            </div>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="status" <?php echo isset($category) && $category->status == 1 ? 'checked' : ''; ?>>
                <label for="status">Is active?</label>
            </label>
        </div>
        <?php if(isset($p) && $p === 'cat-edit'): ?>
            <input type="submit" name="action" class="btn btn-primary" value="Edit">
        <?php else: ?>
            <input type="submit" name="action" class="btn btn-primary" value="Submit">
        <?php endif; ?>
    </form>
</div>
