<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 21.03.2018
 * Time: 13:43
 */
if ($p == 'cat-delete') {
    $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);
    if (empty($ID)) {
        redirect(ADMIN_URL . "category-list");
    }
    $category = Category::find($ID);

    $subCategories = $category->getChildren();
    $rel = $category->getRel();

    $session->message('<div class="alert alert-danger">Category not deleted. Is in use!</div>');

    if (empty($subCategories) && empty($rel)) {
        $category->delete();
        $session->message('<div class="alert alert-success">Category Deleted</div>');
    }

    redirect(ADMIN_URL . "cat-list");
}