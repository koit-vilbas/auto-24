<?php



// LIMIT x,max
// x = (page - 1) * 10
// max = MAX_ROWS

//Page count = results / 10


$search = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);

$search = strtolower($search);

$pageNumber = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);
$page = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);

if(empty($pageNumber))
    $pageNumber = 1;

$pageCount = ceil(Category::count_all() / $options['page_items_no']);
$noOfItems = $options['page_items_no'];

$offset = ($pageNumber - 1) * $noOfItems;
$categories = Category::findByLimit($offset, $noOfItems);
$n = '';
if (empty($search)) {
    $n = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
    if (empty($n)) {

        $categories = Category::findByLimit($offset, $noOfItems);
        $_SESSION['search'] = false;
    } else {
        $categories = Category::findByName($n, $pageNumber);
        $pageCount = ceil(count($categories) / $options['page_items_no']);
    }
} else {
    $_SESSION['search'] = true;
    redirect(ADMIN_URL . 'cat-list'.DS.$pageNumber.DS.$search);
}


$nextPage = 0;
$prevPage = 0;
if(!empty($pageNumber)) {
    $nextPage = $pageNumber + 1;
    $prevPage = $pageNumber - 1;

    if($nextPage > $pageCount)
        $nextPage = $pageNumber;

    if($prevPage < 1)
        $prevPage = $pageNumber;
}
?>
<div class="row">
    <div class="col-12">
        <form method="POST" action="<?= ADMIN_URL . 'cat-list'.DS.$pageNumber ?>">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Search..." name="search" value="<?php echo isset($n) ? $n : ""; ?>">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12 m-md-2">
        <a href="<?php echo ADMIN_URL . 'cat-add/'?>"><button class="btn btn-outline-success" type="submit"><?php t('add_new_category'); ?></button></a>
    </div>
</div>
<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Name</th>
            <th>Parent</th>
            <th>Added</th>
            <th>Status</th>
            <th>Delete</th>
        </thead>
        <tbody>

            <?php if(!empty($categories)) : foreach ($categories as $category) {?>
                <?php $translation = Translations::getTranslations($category, 'category', $session->getLanguage()); ?>
                <tr>
                    <?php $cat_o = Category::find($category->parentID); ?>
                    <td><a href="<?php echo ADMIN_URL . 'cat-edit/' . $category->ID; ?>"><?php echo $translation[0]->translation; ?></td>
                    <?php $translation = Translations::getTranslations($cat_o, 'category', $session->getLanguage()); ?>
                    <td><?php echo empty($category->parentID) ? 'None' : $translation[0]->translation; ?></td>
                    <td><?php echo $category->added; ?></td>
                    <td><?php echo $category->status; ?></td>
                    <td><a href="<?php echo ADMIN_URL . 'cat-delete/' . $category->ID?>"><i class="fa fa-trash"></i></a></td>
                </tr>
            <?php } endif; ?>
        </tbody>
    </table>
</div>

<nav aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$prevPage.DS.$n ?>"><?= t('previous') ?></a></li>
        <?php for($i=1;$i<=$pageCount;$i++):?>
            <li class="page-item <?= $pageNumber == $i ? 'active' : '' ?>"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$i.DS.$n ?>"><?= $i ?></a></li>
        <?php endfor; ?>
        <li class="page-item"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$nextPage.DS.$n ?>"><?= t('next') ?></a></li>
    </ul>
</nav>
