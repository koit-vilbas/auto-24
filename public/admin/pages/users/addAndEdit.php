<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 15.03.2018
 * Time: 11:06
 */
    $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $repeatPassword = filter_input(INPUT_POST, 'repeatPassword', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $group = filter_input(INPUT_POST, 'group', FILTER_SANITIZE_STRING);
    $birthday = filter_input(INPUT_POST, 'birthday', FILTER_SANITIZE_STRING);
    $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);

    if($p === 'user-edit') {
        if(empty($_GET['ID'])) {
            redirect();
        }
        $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

        $user = User::find($ID);
    }

    if(isset($action) && $action === t('SUBMIT_BUTTON', true)) {
        $user = new User();
        $user->username = $username;
        $user->password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
        $user->email = $email;
        $user->name = $name;
        $user->userGroup = $group;
        $user->birthDate = $birthday;
        $user->status = isset($status) && $status === 'on' ? 1 : 0;
        $user->added = date("Y-m-d H:i:s");
        $user->addedBy = 1;
        $user->changedBy = 1;

        if(User::findByUsername($username)) {
            echo '<div class="alert alert-danger">'. t('USERNAME_TAKEN', true) .'</div>';
            unset($user);
        } else {
            if($user->save()) {
                echo '<div class="alert alert-success">'. t('USER_ADDED', true) .'</div>';
            } else {
                echo '<div class="alert alert-danger">'. t('USER_NOT_ADDED', true) .'</div>';
            }
        }

    }

if(isset($action) && $action === t('EDIT_BUTTON', true)) {
    if($password === $repeatPassword) {
        $user->password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
    }
    $user->email = $email;
    $user->name = $name;
    $user->userGroup = $group;
    $user->birthDate = $birthday;
    $user->status = isset($status) && $status === 'on' ? 1 : 0;
    $user->changedBy = 1;

    if($user->save()) {
        echo '<div class="alert alert-success">' . t('USER_EDITED', true) . '</div>';
    } else {
        echo '<div class="alert alert-danger">' . t('USER_NOT_EDITED', true) . '</div>';
    }


}
?>
<div class="container">
    <a href="<?php echo ADMIN_URL . 'user-list'?>" class="btn btn-dark"><i class="fa fa-chevron-circle-left"></i>Tagasi</a>
    <form method="post">
        <div class="row form-group">
            <label for="addUser_username" class="col-2 col-form-label"><?php t('USERNAME'); ?>:</label>
            <div class="col-10">
                <input type="text" name="username" class="form-control" id="addUser_username" required <?php echo $p === 'user-edit' ? 'disabled' : ''; ?> value="<?php echo isset($user) ? $user->username : ''; ?>">
            </div>
        </div>
        <div class="row form-group">
            <label for="addUser_password" class="col-2 col-form-label"><?php t('PASSWORD'); ?>:</label>
            <div class="col-10">
                <input type="password" name="password" class="form-control" id="addUser_password" <?php echo $p === 'user-edit' ? '' : 'required'; ?>>
            </div>
        </div>
        <div class="row form-group">
            <label for="addUser_repeatPassword" class="col-2 col-form-label"><?php t('REPEAT_PASSWORD'); ?>:</label>
            <div class="col-10">
                <input type="password" name="repeatPassword" class="form-control" id="addUser_repeatPassword" <?php echo $p === 'user-edit' ? '' : 'required'; ?>>
            </div>
        </div>
        <div class="row form-group">
            <label for="addUser_email" class="col-2 col-form-label"><?php t('EMAIL'); ?>:</label>
            <div class="col-10">
                <input type="email" name="email" class="form-control" id="addUser_email" value="<?php echo isset($user) ? $user->email : ''; ?>" >
            </div>
        </div>
        <div class="row form-group">
            <label for="addUser_name" class="col-2 col-form-label"><?php t('NAME'); ?>:</label>
            <div class="col-10">
                <input type="text" name="name" class="form-control" id="addUser_name" value="<?php echo isset($user) ? $user->name : ''; ?>" >
            </div>
        </div>
        <div class="row form-group">
            <label for="addUser_group" class="col-2 col-form-label"><?php t('GROUP'); ?>:</label>
            <div class="col-10">
                <select class="form-control" id="addUser_group" name="group">
                    <option value="user" <?php echo isset($user) && $user->userGroup === 'user' ? 'selected' : ''?>><?php t('GROUP_USER'); ?></option>
                    <option value="moderator" <?php echo isset($user) && $user->userGroup === 'moderator' ? 'selected' : ''?>><?php t('GROUP_MODERATOR'); ?></option>
                    <option value="admin" <?php echo isset($user) && $user->userGroup === 'admin' ? 'selected' : ''?>><?php t('GROUP_ADMIN'); ?></option>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <label for="addUser_birthDay" class="col-2 col-form-label"><?php t('BIRTHDAY'); ?>:</label>
            <div class="col-10">
                <input type="date" name="birthday" min="1000-01-01" max="3000-12-31" class="form-control" id="addUser_birthDay" value="<?php echo isset($user) ? $user->birthDate : ''; ?>">
            </div>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="status" <?php echo isset($user) && $user->status == 1 ? 'checked' : ''; ?>>
                <label for="status"><?php t('IS_ACTIVE'); ?>?</label>
            </label>
        </div>
        <?php if(isset($p) && $p === 'user-edit'): ?>
            <input type="submit" name="action" class="btn btn-primary" value="<?php t('EDIT_BUTTON'); ?>">
        <?php else: ?>
            <input type="submit" name="action" class="btn btn-primary" value="<?php t('SUBMIT_BUTTON'); ?>">
        <?php endif; ?>
    </form>
</div>

