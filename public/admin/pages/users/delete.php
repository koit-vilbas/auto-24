<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 15.03.2018
 * Time: 11:06
 */

if($p === 'user-delete') {
    if(empty($_GET['ID'])) {
        redirect(ADMIN_URL . 'user-list');
    }
    $ID = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);

    if($session->user_id == $ID) {
        $session->message(t('CANNOT_DELETE_SELF'));
        redirect(ADMIN_URL . 'user-list', 3);
        exit();
    }

    $user = User::find($ID);
    $user->delete();
    $session->message(t('USER_DELETED'));
    redirect(ADMIN_URL . 'user-list', 3);
}