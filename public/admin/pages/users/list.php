<?php

$search = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_STRING);

$search = strtolower($search);

$pageNumber = filter_input(INPUT_GET, 'ID', FILTER_VALIDATE_INT);
$page = filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING);

if(empty($pageNumber))
    $pageNumber = 1;

$pageCount = ceil(User::count_all() / $options['page_items_no']);
$noOfItems = $options['page_items_no'];

$offset = ($pageNumber - 1) * $noOfItems;
$users = User::find_all($pageNumber);
$n = "";
if (empty($search)) {
    $n = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
    if (empty($n)) {
        $users = User::find_all($pageNumber);
        $_SESSION['search'] = false;
    } else {
        $users = User::searchByUsername($n, $pageNumber);
        $pageCount = ceil(count($users) / $options['page_items_no']);
    }
} else {
    $_SESSION['search'] = true;
    redirect(ADMIN_URL . 'user-list'.DS.$pageNumber.DS.$search);
}

$nextPage = 0;
$prevPage = 0;
if(!empty($pageNumber)) {
    $nextPage = $pageNumber + 1;
    $prevPage = $pageNumber - 1;

    if($nextPage > $pageCount)
        $nextPage = $pageNumber;

    if($prevPage < 1)
        $prevPage = $pageNumber;
}

?>
<div class="row">
    <div class="col-12">
        <form method="POST" action="<?= ADMIN_URL . 'user-list'.DS.$pageNumber ?>">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Search..." name="search" value="<?php echo isset($n) ? $n : ""; ?>">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12 m-md-2">
        <a href="<?php echo ADMIN_URL . 'user-add/'?>"><button class="btn btn-outline-success" type="submit"><?php t('add_new_user'); ?></button></a>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th><?php t('USERNAME'); ?></th>
                    <th><?php t('EMAIL'); ?></th>
                    <th><?php t('NAME'); ?></th>
                    <th><?php t('BIRTHDAY'); ?></th>
                    <th><?php t('ADDED'); ?></th>
                    <th><i class="fa fa-check"></i</th>
                    <th><i class="fa fa-trash"></th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($users)) : foreach($users as $user) { ?>
                    <tr>
                        <td><a href="<?php echo ADMIN_URL . 'user-edit/' . $user->ID; ?>"><?php echo $user->username;?></a></td>
                        <td><?php echo $user->email;?></td>
                        <td><?php echo $user->name;?></td>
                        <td><?php echo showDate($user->birthDate);?></td>
                        <td><?php echo showDateTime($user->added);?></td>
                        <td><?php echo $user->status == 1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';?></td>
                        <td><a href="<?php echo ADMIN_URL . 'user-delete/' . $user->ID?>"><i class="fa fa-trash"></i></a></td>
                    </tr>
                <?php } endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<nav aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$prevPage.DS.$n ?>"><?= t('previous') ?></a></li>
        <?php for($i=1;$i<=$pageCount;$i++):?>
            <li class="page-item <?= $pageNumber == $i ? 'active' : '' ?>"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$i.DS.$n ?>"><?= $i ?></a></li>
        <?php endfor; ?>
        <li class="page-item"><a class="page-link" href="<?= ADMIN_URL.$page.DS.$nextPage.DS.$n ?>"><?= t('next') ?></a></li>
    </ul>
</nav>


