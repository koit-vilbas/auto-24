<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 05.09.2018
 * Time: 12:15
 */
require_once "../include/start.php";

//pd();

$makes = Make::get($_POST['make_ids']);

$mappedModels = [];

foreach($makes as $makeKey => $make) {
    // Get models for makes
    $models = Model::findByMake($make->ID);

    $mappedModels[$makeKey]['text'] = $make->title;

    foreach($models as $key => $model) {
        $mappedModels[$makeKey]['children'][$key]['id'] = $model->ID;
        $mappedModels[$makeKey]['children'][$key]['text'] = $model->title;
    }
}



echo json_encode($mappedModels);
