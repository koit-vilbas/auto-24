<?php

require_once '../include/start.php';

$lang = filter_input(INPUT_GET, 'lang', FILTER_SANITIZE_STRING);

if(in_array($lang, ['et', 'en'])) {
    $session->setLanguage($lang);
    header("Refresh:0");
}
header("Refresh:0");