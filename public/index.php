<?php

require_once "../include/start.php";
get_template('head');

$search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);

if(empty($search)) {
    $cars = Car::find_all();
} else {
    $cars = Car::findByName($search);
}

//$cars = Car::find_all();
$counter = 0;

//$picturesPathMain = UPLOAD_PATH . $car->ID . DS;


//$pictures = glob($picturesPathThumb . "/*.{jpg,png}", GLOB_BRACE);

$makes = Make::get();

$categories = Category::find_all();

$carSubCategoriesId = [];

$carSubCategories = Rel::find_all();
if(is_array($carSubCategories)) {
    $carSubCategoriesId = array_column($carSubCategories, 'categoryID');
}

$categoryFilters = isset($_POST['category']) ? $_POST['category'] : null;
$categorySelectFilters = isset($_POST['category_select']) ? $_POST['category_select'] : null;

if(is_array($categoryFilters)) {
    $_SESSION['category_filters'] = $categoryFilters;

    $rels = Rel::find_all();
    $mappedRelsByCarID = [];
    foreach ($rels as $rel) {
        $mappedRelsByCarID[$rel->carID][] = $rel;
    }

    $mappedRelsByCategoryID = [];
    foreach ($rels as $rel) {
        $mappedRelsByCategoryID[$rel->categoryID] = $rel;
    }

    // Get only categories which are set
    $activeCategories = [];

    foreach ($categoryFilters as $key => $filter) {
        if(!empty($filter)) {
            $activeCategories[$key] = $filter;
        }
    }

    foreach ($cars as $key => $car) {
        $carRels = $mappedRelsByCarID[$car->ID];

        foreach ($carRels as $carRel) {
            if(array_key_exists($carRel->categoryID, $activeCategories)) {
                // Car has given category value
                if(strpos($carRel->value, $activeCategories[$carRel->categoryID]) !== false) {
                    //Match
                } else {
                    // No match, remove car from list
                    unset($cars[$key]);
                }
            }

        }
    }



}

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Auto 24</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/"><?php t('HOME') ?> <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <div class="col-2">
            <select name="language" id="language" class="form-control">
                <option value="et" <?php echo $session->getLanguage() === 'et' ? 'selected' : '' ?>><?php t('estonian') ?></option>
                <option value="en" <?php echo $session->getLanguage() === 'en' ? 'selected' : '' ?>><?php t('english') ?></option>
            </select>
        </div>

        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" value="<?= $search ?>" type="search" name="search" placeholder="<?php t('SEARCH') ?>" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><?php t('SEARCH') ?></button>
        </form>
    </div>
</nav>

<div class="container-fluid" style="padding:0;">
    <div class="row">
        <div class="col-md-9" style="padding:0; display: flex; flex-wrap: wrap; align-content: flex-start;">
            <?php if(!empty($cars)) { foreach ($cars as $car): ?>
                <?php
                $mainPicture = Car::getMainPicturePath($car->ID);
                $picturesPathThumb = UPLOAD_PATH . $car->ID . DS . THUMB . DS;
                $picturesPathMedium = UPLOAD_PATH . $car->ID . DS . MEDIUM . DS;


                $thumbPath = $picturesPathThumb . $mainPicture;
                $mediumPath = $picturesPathMedium . $mainPicture;

                ?>
                <div class="img-group">
                    <a href="<?php echo CAR_URL . $car->url; ?>">
                        <img src="<?php echo (new Picture())->picturePathToUrl($mediumPath); ?>" class="img-fluid img-custom-1">
                        <div class="overlay "><?php echo $car->name; ?></div>
                    </a>

                </div>

            <?php endforeach; } else {
                echo "<div class='alert alert-warning m-4'>". t('NO_CARS', true) ."</div>";
            } ?>
        </div>

        <div class="col-md-3 shadow-left" style="">
            <form method="post" action="<?= ROOT_URL ?>">
            <?php if(!empty($categories)): ?>
                <?php foreach($categories as $category): ?>
                    <?php if($category->getChildren() == false && $category->parentID == 0 || true):?>
                        <?php $translation = Translations::getTranslations($category, 'category', $session->getLanguage()); ?>
                        <div class="row">
                            <label for="addCar_<?php echo $translation[0]->translation; ?>" class="col-2 col-form-label"><?php echo $translation[0]->translation; ?></label>
                        </div>
                        <div class="row">
                            <div class="col-10">
                                <?php $options = Rel::find_by_query('SELECT DISTINCT value FROM rels WHERE categoryID = ' . $category->ID) ?>
                                <select name="category[<?php echo $category->ID; ?>]" class="form-control">
                                    <option value=""></option>
                                    <?php foreach($options as $option): if (!empty($option->value)) { ?>
                                    <option value="<?= $option->value ?>"><?= $option->value ?></option>
                                    <?php } endforeach; ?>
                                </select>
<!--                                <input type="text"  id="addCar_--><?php //echo $translation[0]->translation; ?><!--" value="">-->
                            </div>
                        </div>
                        <?php //if(!empty($category->getChildren())) { pd($category->getChildren()); } ?>
                    <?php elseif($category->getChildren() && false): ?>
                        <?php $translation = Translations::getTranslations($category, 'category', $session->getLanguage()); ?>
                        <?php $children = $category->getChildren(); ?>
                        <div class="row">
                            <label for="addCar_<?php echo $translation[0]->translation; ?>" class="col-2 col-form-label"><?php echo $translation[0]->translation; ?></label>
                        </div>
                        <div class="row">
                            <div class="col-10">
                                <select class="form-control" id="addCar_<?php echo $translation[0]->translation; ?>" name="category_select[<?php echo $category->ID; ?>]">
                                    <?php foreach($children as $child): ?>
                                        <option value="<?php echo $child->ID; ?>" <?php echo in_array($child->ID, $carSubCategoriesId) ? 'selected' : "" ?>><?php echo $child->name; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
                <input type="submit" name="action" class="btn btn-primary my-2" value="<?php t('SUBMIT_BUTTON');?>">
            </form>
        </div>


    </div>

</div>
<?php get_template('footer'); ?>