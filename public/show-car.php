<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 30.05.2018
 * Time: 11:28
 */

require_once "../include/start.php";

$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
$url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_STRING);

if (empty($page) || $page != 'car' || empty($url)) {
    redirect(MAIN_URL);
}

$car = Car::getByUrl($url);
if (empty($car)) {
    redirect(MAIN_URL);
}

$relations = CategoryRelation::byCar($car);

$categories = Category::find_all();


//get all main categories
//$mainCategories = Category::getParents();

get_template('head');
$mainPicture = Car::getMainPicturePath($car->ID);
$picturesPathThumb = UPLOAD_PATH . $car->ID . DS;
$thumbPath = $picturesPathThumb . $mainPicture;
?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Auto 24</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/"><?php t('HOME') ?> <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
<div class="container-fluid">
    <div class="row no-gutters">
        <div class="col-4">
            <table class="table table-striped table-hover table-bordered">
                <tbody>
                <tr>
                    <th class="text-right"><?php t('CAR_NAME') ?></th>
                    <td><?= $car->name ?></td>
                </tr>
                <tr>
                    <th class="text-right"><?php t('CAR_POWER') ?></th>
                    <td><?= $car->power ?>kW</td>
                </tr>
                <tr>
                    <th class="text-right"><?php t('CAR_PRICE') ?></th>
                    <td><?= $car->price ?>€</td>
                </tr>
                <tr>
                    <th class="text-right"><?php t('CAR_COLOR') ?></th>
                    <td><?= $car->color ?></td>
                </tr>
                <tr>
                    <th class="text-right"><?php t('CAR_DOORCOUNT') ?></th>
                    <td><?= $car->doorCount ?></td>
                </tr>
                <?php foreach($relations as $relation): ?>
                    <?php $translations = Translations::getTranslations($relation, 'category',  $session->getLanguage()); ?>
                    <tr>
                        <th class="text-right"><?= $translations[0]->translation?></th>
                        <td><?= $relation->value ?></td>
                    </tr>
                <?php endforeach; ?>


                </tbody>
            </table>
        </div>
        <div class="col-7">
            <img class="img-fluid" src="<?php echo (new Picture())->picturePathToUrl($thumbPath); ?>">
        </div>
        <div class="col-1">
            <?php $pictures = $car->getPictures(); ?>
            <?php foreach ($pictures['thumb'] as $picture): ?>
                <img src="<?php echo (new Picture())->picturePathToUrl($picture); ?>" class="img-fluid shadow">
            <?php  endforeach;?>
        </div>
    </div>
</div>

<?php get_template('footer');