<?php
/**
 * Created by PhpStorm.
 * User: janek.mander
 * Date: 10.05.2016
 * Time: 9:09
 */
?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo TEMPLATE_URL_JS; ?>bootstrap.min.js"></script>
<script defer src="<?php echo TEMPLATE_URL_JS; ?>fontawesome-all.js"></script>
<script>
    var mainUrl = '<?php echo MAIN_URL; ?>';
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="<?php echo TEMPLATE_URL_JS; ?>app.js"></script>

<?php if (!empty($p) && isset($p['js'])) : ?>
    <script src="<?php echo $p['js']; ?>"></script>
<?php endif; ?>
</body>
</html>
