$(document).ready(function() {
    var height = $('.container-fluid').css('height');
    var id;
    $(window).resize(function() {
        clearTimeout(id);
        id = setTimeout(doneResizing, 500);
    });
    function doneResizing() {
        var newheight = $('.container-fluid').css('height');
        if (newheight != height) {
            $('img').fadeOut();
            $('img').fadeIn();
            height = newheight;
        }
    }

    var filterDialogOpen = false;

    $('#filter-button').click(function () {
        if(!filterDialogOpen) {
            $('.filter').toggle('fast');
            filterDialogOpen = true;
        }
    });

    $('#filter-close-button').click(function () {
        console.log('clicked');
        if(filterDialogOpen) {
            $('.filter').toggle('fast');
            filterDialogOpen = false;
        }
    });



    $('#makeSelect').change(function () {
        //Load models
        $.ajax({
            type: 'post',
            url: '/ajax-models.php',
            data: {
                make_ids: $('#makeSelect').val()
            }
        }).done(function (data) {

            console.log(data);

            $('#modelSelect').select2({
                data: JSON.parse(data)
            });
        });


    });

    $('#makeSelect').select2({
        width: '100%',
        closeOnSelect: false
    });

    $('#language').change(function () {
        $.ajax({
            url: mainUrl + 'change-language.php?lang=' + $(this).val(),
            success: function () {
                location.reload();
            }
        });
    });
});