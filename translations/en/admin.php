<?php

$translations = [
    'USERNAME' => 'Username',
    'PASSWORD' => 'Password',
    'REPEAT_PASSWORD' => 'Repeat Password',
    'EMAIL' => 'Email',
    'NAME' => 'Name',
    //Group translations
    'GROUP' => 'Group',
    'GROUP_USER' => 'User',
    'GROUP_MODERATOR' => 'Moderator',
    'GROUP_ADMIN' => 'Administrator',

    'BIRTHDAY' => 'Birthday',
    'IS_ACTIVE' => 'Is Active',
    'USERNAME_TAKEN' => 'Username is taken.',

    //User translations
    'USER_ADDED' => 'User added.',
    'USER_NOT_ADDED' => 'User NOT added.',
    'USER_EDITED' => 'User edited.',
    'USER_NOT_EDITED' => 'User NOT edited.',

    //Button translations
    'EDIT_BUTTON' => 'Edit',
    'BACK_BUTTON' => 'Back',
    'SUBMIT_BUTTON' => 'Submit',

    'ADDED' => 'Added',
    'STATUS' => 'Status',
    'DELETE' => 'Delete',

    'CAR_POWER' => 'Power',
    'CAR_DOOR_COUNT' => 'Doors',
    'CAR_COLOR' => 'Color',
    'CAR_PRICE' => 'Price',

    'CAR_EDITED' => 'Car edited.',
    'CAR_ADDED' => 'Car added.',
    'CAR_NOT_ADDED' => 'Car NOT added.',

    'GALLERY' => 'Gallery',
    'add_new_user' => 'Add New User',
    'add_new_category' => 'Add New Category',
    'add_new_car' => 'Add New Car',
    'Car deleted.' => 'Car deleted.',
    'Error: car not deleted.' => 'Error: car not deleted.',

    'previous' => 'Previous',
    'next' => 'Next',

    'Users' => 'Users',
    'Admin Dashboard' => 'Admin Dashboard',
    'Categories' => 'Categories',
    'Cars' => 'Cars',

    'estonian' => 'Estonian',
    'english' => 'English',

    'Add New User' => 'Add New User',
    'Edit User' => 'Edit User',
    'Add category' => 'Add category',
    'Edit category' => 'Edit category',
    'Add car' => 'Add car',
    'Edit Car' => 'Edit Car',
    'Car Gallery' => 'Car Gallery',









];