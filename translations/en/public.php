<?php
/**
 * Created by PhpStorm.
 * User: koit.vilbas
 * Date: 30.05.2018
 * Time: 13:03
 */

$translations = [
    'VIEW' => 'View',
    'CAR_NAME' => 'Name',
    'SUBMIT_BUTTON' => 'Submit',
    'HOME' => 'Home',
    'SEARCH' => 'Search',
    'NO_CARS' => 'No cars found.',

    'CAR_POWER' => 'Power',
    'CAR_PRICE' => 'Price',
    'CAR_COLOR' => 'Color',
    'CAR_DOORCOUNT' => 'Door count',

    'estonian' => 'Estonian',
    'english' => 'English',
];