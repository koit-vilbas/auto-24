<?php
$translations = [
    'VIEW' => 'Vaade',
    'CAR_NAME' => 'Nimi',
    'CAR_POWER' => 'Võimsus',
    'CAR_PRICE' => 'Hind',
    'CAR_COLOR' => 'Värv',
    'CAR_DOORCOUNT' => 'Uste arv',

    'SUBMIT_BUTTON' => 'Esita',
    'HOME' => 'Avaleht',
    'SEARCH' => 'Otsing',
    'NO_CARS' => 'Autosid ei leitud.',
    'BACK_BUTTON' => 'Tagasi',

    'estonian' => 'Eesti',
    'english' => 'Inglise',

];